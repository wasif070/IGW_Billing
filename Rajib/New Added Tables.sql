DROP TABLE IF EXISTS `compared_data`;
CREATE TABLE `compared_data` (
  `id` int(11) NOT NULL auto_increment,
  `compared_info_id` int(11) NOT NULL,
  `prefix` varchar(40) NOT NULL default '',
  `destination` varchar(30) default NULL,
  `rates` varchar(40) NOT NULL default '',
  `min_rateplan` varchar(40) NOT NULL default '',
  `max_rateplan` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`id`)
)

DROP TABLE IF EXISTS `compared_info`;
CREATE TABLE `compared_info` (
  `id` int(11) NOT NULL auto_increment,
  `selected_ids` varchar(40) NOT NULL default '',
  `compared_time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `table_type` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
)

DROP TABLE IF EXISTS `maximum_rate_prefix_number`;
CREATE TABLE `maximum_rate_prefix_number` (
  `id` int(11) NOT NULL auto_increment,
  `compared_info_id` int(11) NOT NULL,
  `rateplan_id` int(11) NOT NULL,
  `number_of_prefix` int(11) default NULL,
  `table_type` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
)

DROP TABLE IF EXISTS `minimum_rate_prefix_number`;
CREATE TABLE `minimum_rate_prefix_number` (
  `id` int(11) NOT NULL auto_increment,
  `compared_info_id` int(11) NOT NULL,
  `rateplan_id` int(11) NOT NULL,
  `number_of_prefix` int(11) default NULL,
  `table_type` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
)

DROP TABLE IF EXISTS `temp_rateplan`;
CREATE TABLE `temp_rateplan` (
  `rateplan_id` int(11) NOT NULL auto_increment,
  `rateplan_name` varchar(100) NOT NULL,
  `rateplan_status` tinyint(1) NOT NULL default '1',
  `rateplan_create_date` decimal(18,0) NOT NULL,
  PRIMARY KEY  (`rateplan_id`)
)

CREATE TABLE `temp_rates` (
  `id` int(11) NOT NULL auto_increment,
  `rate_id` int(11) NOT NULL default '0',
  `rateplan_id` int(11) NOT NULL default '0',
  `rate_destination_code` varchar(32) NOT NULL,
  `rate_destination_name` varchar(50) NOT NULL,
  `rate_per_min` decimal(10,4) NOT NULL default '0.0000',
  `rate_first_pulse` tinyint(2) NOT NULL default '0',
  `rate_next_pulse` tinyint(2) NOT NULL default '0',
  `rate_grace_period` tinyint(2) NOT NULL default '0',
  `rate_failed_period` tinyint(2) NOT NULL default '0',
  `rate_day` int(1) NOT NULL default '0',
  `rate_from_hour` tinyint(2) NOT NULL default '0',
  `rate_from_min` tinyint(2) NOT NULL default '0',
  `rate_to_hour` tinyint(2) NOT NULL default '0',
  `rate_to_min` tinyint(2) NOT NULL default '0',
  `rate_created_date` decimal(18,0) NOT NULL,
  `rate_delete_time` decimal(18,0) default '0',
  `delete_time` decimal(18,0) default '0',
  `rate_status` tinyint(1) NOT NULL default '0',
  `rate_delete` tinyint(1) NOT NULL default '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
)